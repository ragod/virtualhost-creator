# Author : Rahul Godiyal
# Version   : 2.0

xamppPath = input('Enter the xampp folder path : ')
projectPath = input('Enter the project folder path : ')
domainName = input('Enter the domain name : ')

# Set xampp hosts file
filePath = str(xamppPath) + "\\apache\conf\extra\httpd-vhosts.conf"
file = open(filePath, "a")
file.write('\n \n <VirtualHost *:80>'
    '\n \t ServerAdmin webmaster@'+ str(domainName) +
    '\n \t DocumentRoot "'+ str(projectPath) +'"'
    '\n \t ServerName '+ str(domainName) +
    '\n \t ErrorLog "logs/'+ str(domainName) +'-error.log"'
    '\n \t CustomLog "logs/'+ str(domainName) +'-access.log" common'
'\n </VirtualHost>')

# Set hosts file of windows 
newFilePath = "C:\Windows\System32\drivers\etc\hosts"
newFile = open(newFilePath, "a")
newFile.write(' ' + domainName)

print('Done')