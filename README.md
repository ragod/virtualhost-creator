## Virtualhost Creator
**Author : Rahul Godiyal**

**Version : 2.0**

It will create virtualhost for you.

**Requirements:**
Install python in your system.

**Execution:**
Open cmd (command prompt/ Terminal) as administrator

**Write command:**

    python virtualhost.py

**Example:**

    python virtualhost.py
    
    Enter the xampp folder path : C:\xampp
    Enter the project folder path : C:\xampp\htdocs\project
    Enter the domain name : project.in
    Done